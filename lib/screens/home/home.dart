import 'package:crash/screens/home/text_section.dart';
import 'package:flutter/material.dart';

import 'image_banner.dart';

class LocationDetails extends StatelessWidget {
  const LocationDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
          body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Expanded(
            child: ImageBanner(assetPath: "assets/images/pagoda.jpg"),
            flex: 4,
          ),
          Flexible(
          flex: 4,
            child: Column(children: [
              const Center(
                child: Text(
                  "TITLE",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              ElevatedButton.icon(
                  onPressed: () {},
                  icon: const Icon(Icons.ac_unit_sharp),
                  label: const Text("HEllo there")),
              Center(
                child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.fromLTRB(30, 10, 30, 10)),
                    child: const Text(
                      "Press ME",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ]),
          ),
          Flexible(
              child: ListView(
                children: const [
                  TextSection(
                      title: "Pagoda", body: "Beautiful place in Japan"),
                  TextSection(
                      title: "Pagoda", body: "Beautiful place in Japan"),
                  TextSection(
                      title: "Pagoda", body: "Beautiful place in Japan"),
                  TextSection(
                      title: "Pagoda", body: "Beautiful place in Japan"),
                  TextSection(
                      title: "Pagoda", body: "Beautiful place in Japan"),
                  TextSection(
                      title: "Pagoda", body: "Beautiful place in Japan"),
                  TextSection(
                      title: "Pagoda", body: "Beautiful place in Japan"),
                ],
              ),
              flex: 4,
              fit: FlexFit.loose)
        ],
      ));
}
