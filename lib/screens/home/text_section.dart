import 'package:flutter/material.dart';

class TextSection extends StatelessWidget {
  final String title;
  final String body;
  static const double hpad = 16.0;

  const TextSection({Key? key, required this.title, required this.body})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(hpad, 32.0, hpad, 4.0),
          child: Text(title,style: const TextStyle(fontWeight: FontWeight.bold),),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(hpad, 10.0, hpad, hpad),
          child: Text(body),
        ),
      ],
    );
  }
}
