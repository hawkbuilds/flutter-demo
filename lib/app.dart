import 'package:crash/screens/home/home.dart';
import 'package:crash/themes.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  final apptheme = AppTheme();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: apptheme.light,
        darkTheme: apptheme.light,
        home: Container(
          child: const LocationDetails(),
          decoration: const BoxDecoration(color: Colors.black),
        ));
  }
}
